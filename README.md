MySQL Audit
=========

This role will manage the installation and configuration of the Audit Plugin for MySQL from McAfee.


Requirements
------------

Ansible v2.4.x

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

Nil.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: ansible-role-mysql-audit, x: 42 }

License
-------

BSD

Author Information
------------------

Aaron Guise

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
